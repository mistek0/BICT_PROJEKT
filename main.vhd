----------------------------------------------------------------------------------
-- Company:     VUT 
-- Engineers:   Marek and Beno
-- 
-- Create Date:    13:07:26 03/21/2018 
-- Design Name:    
-- Module Name:    main - Behavioral 
-- Project Name:   BCD and BCD+3 converter
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;    -- Potrebna kniznica pre operacie + a -
USE ieee.numeric_std.ALL;           -- Potrebna kniznica pre indexovanie

-- Input/output description
entity top is 
    port (
    SW_EXP: in std_logic_vector(15 downto 0);   -- Inicializacia prepinacov rozsirujucej periferie
    LED_EXP: out std_logic_vector(15 downto 0); -- Inicializacia LED rozsirujucej periferie
	 LEDS: out std_logic_vector(3 downto 0);    -- Inicializacia LED Coolrunner II
    D_POS: out std_logic_vector(3 downto 0);    -- Inicializacia pozicie 7-segmentovych displejov
    D_SEG: out std_logic_vector(6 downto 0);    -- Inicializacia jednotlivych segmentov displeja
    CLK: in std_logic                           -- Inicializacia vnutornych hodin
    );
end top;

--Internal structure description
architecture Behavioral of top is
    signal input: std_logic_vector(7 downto 0):= (others => '0');   --Nastavenie vstupnej premennej
    signal input_old: std_logic_vector(7 downto 0):= (others => '1'); -- Nastavenie pomocnej vstupnej premennej
    signal temp_reading: std_logic_vector(7 downto 0);  --Pomocna premenna o 8 bitoch
    signal huns: std_logic_vector(3 downto 0);          -- premenna Stoviek
    signal tens: std_logic_vector(3 downto 0);          -- premenna Desiatok
    signal ones: std_logic_vector(3 downto 0);          -- premenna Jednotiek
    signal clk_10: std_logic := '0';                    -- premenna casu
    signal tmp_16: std_logic_vector(11 downto 0) := x"000"; -- pomocna pre prisposobenie casu
    signal pos: std_logic_vector(3 downto 0) := "1011";     -- pomocna premenna pozicie displejov
    signal display: std_logic_vector(3 downto 0) := "0000"; -- pomocna premenna segmentov displeja
    signal SWITCH: std_logic_vector(3 downto 0) := "0000";
           
begin

     -- Nacitanie vstupov a priradenie vystupov k premennym
	 input(7 downto 0) <= SW_EXP(15 downto 8);
	 SWITCH(3 downto 0) <= SW_EXP(3 downto 0);
    LED_EXP(7 downto 0) <= NOT SW_EXP(15 downto 8);
    LED_EXP(15 downto 8) <=  SW_EXP(15 downto 8);
	 LEDS(0) <= NOT SWITCH(3);
	 LEDS(1) <= NOT SWITCH(2);
	 LEDS(2) <= NOT SWITCH(1);
    LEDS(3) <= NOT SWITCH(0);
	 
    -- Process prisposobenia casu
    process(CLK)
    begin
        if rising_edge(CLK) then
            tmp_16 <= tmp_16 + 1;
            if tmp_16 = x"16" then
                tmp_16 <= (others => '0');
                clk_10 <= not clk_10;
            end if;
        end if;       
    end process;
	 
	 
  
    
    -- READING
    process(CLK)
    begin
		if SWITCH(0)= '1' then          -- Prevod binarneho cisla do decimalneho
        if rising_edge(CLK) then             
                if input /= input_old then  -- Kontrola zmeny
                    input_old <= input;                
                    temp_reading<= (others => '0'); -- Nulovanie pomocnych
						  ones <= (others => '0');
						  tens <= (others => '0');
                    huns <= (others => '0');
            
                elsif temp_reading = input then     -- Kontrola zmeny
                    input_old <= input;
                
                else
                    temp_reading <= temp_reading + 1 ; -- Citanie cisla
                    ones <= ones + 1;
                
                    if ones = 9 then        -- Pretecenie jednotiek
                        tens <= tens + 1;
                        ones <= (others => '0');
                        if tens = 9 then    -- Pretecenie desiatok
                            huns <= huns + 1;
                            tens <= (others => '0');
                            ones <= (others => '0');
                        end if;
                    end if;
                end if;
              end if;
				  
		elsif SWITCH(0) = '0' then    -- BCD + 3
                         
                if input /= input_old then
                    input_old <= input;                
                    temp_reading<= (others => '0');
						  ones <= ("0011");
						  tens <= (others => '0');
                          huns <= (others => '0');
            
                elsif temp_reading = input then
                      input_old <= input;
                
                else
                    temp_reading <= temp_reading + 1 ;
                    ones <= ones + 1;
                
                    if ones = 9 then
                        tens <= tens + 1;
                        ones <= (others => '0');
                        if tens = 9 then
                            huns <= huns + 1;
                            tens <= (others => '0');
                            ones <= (others => '0');
                        end if;
                    end if;
                end if;
              end if;
    end process;
	 
	 

-- Volba pozicie pre zobrazenie              
    process(clk_10)
    begin
        if rising_edge(clk_10) then
            if pos = "1011" then 
               pos <= "1101";
            elsif pos = "1101" then
                  pos <= "1110";
            else  pos <= "1011";
            end if;
        end if;
    end process;

-- Vykreslovanie
    process(pos)
    begin
        if pos="1011" then       -- Na prvy digit sa zobrazuju stovky
            display <= huns;
        elsif pos = "1101" then  -- Na druhy digit sa zobrazuju desiatky
             display <= tens;
        elsif pos = "1110" then  -- Na treti digit sa zobrazuju jednotky
            display <= ones;
        end if;  
    end process; 


    

    D_POS <= pos;
    
    with display select                        -- 0        0
    D_SEG <=        "1000000" when "0000", 
                    "1111001" when "0001",     -- 1       ---
                    "0100100" when "0010",     -- 2    5 | 6 | 1
                    "0110000" when "0011",     -- 3       ---
                    "0011001" when "0100",     -- 4    4 |   | 2
                    "0010010" when "0101",     -- 5       ---
                    "0000010" when "0110",     -- 6        3
                    "1111000" when "0111",            
                    "0000000" when "1000",
                    "0011000" when "1001",
                    "1000000" when others;
                    
end Behavioral;